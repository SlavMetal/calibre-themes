# calibre-themes
calibre-themes

This is a repo where I'll dump my Calibre reader themes. I like 
to have a few options, so I'll post whatever alternatives I come up with. 

## Installation

To use, just copy the contents of any of the css files into your Calibre
ebook viewer settings. Just open up an ebook, click `Preferences`, then 
the `User stylesheet` tab. Paste in the contents of the file. Then, just
save and admire how cool you are.
